package com.restapi.rest;

import com.restapi.entity.Employee;
import com.restapi.service.EmployeeService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/employees")
public class EmployeeRestController {
    private EmployeeService employeeService;

    public EmployeeRestController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping()
    public List<Employee> getEmployees() {
        return employeeService.findAll();
    }

    @GetMapping("/{employeeID}")
    public Employee getAnEmployeeById(@PathVariable int employeeID) {
        Employee employee = employeeService.getAnEmployeeByID(employeeID);
        if (employee == null) {
            throw new RuntimeException("Employee with ID" + " " + employeeID + " " + "not found");
        }
        return employee;
    }

    @PostMapping()
    public Employee save(@RequestBody Employee employee) {
        employee.setId(0);
        Employee dbEmployee = employeeService.save(employee);
        return dbEmployee;
    }

    @PutMapping()
    public Employee update(@RequestBody Employee employee) {
        Employee dbEmployee = employeeService.save(employee);
        return dbEmployee;
    }

    @DeleteMapping("/{employeeID}")
    public String delete(@PathVariable int employeeID) {
        Employee employee = employeeService.getAnEmployeeByID(employeeID);
        if (employee == null) {
            throw new RuntimeException("Employee with ID" + " " + employeeID + " " + "not found");
        }
        employeeService.deleteById(employeeID);
        return "Deleted employee with ID" + " " + employeeID;
    }
}
