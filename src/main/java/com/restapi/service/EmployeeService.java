package com.restapi.service;

import com.restapi.entity.Employee;

import java.util.List;


public interface EmployeeService {
    List<Employee> findAll();

    Employee getAnEmployeeByID(Integer id);

    Employee save(Employee employee);

    void deleteById(int id);


}
